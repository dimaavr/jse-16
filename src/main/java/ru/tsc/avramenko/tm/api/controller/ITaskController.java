package ru.tsc.avramenko.tm.api.controller;

import ru.tsc.avramenko.tm.model.Task;

public interface ITaskController {

    void showList();

    void showFindTask(Task task);

    void clearTasks();

    void createTask();

    void showById();

    void showByName();

    void showByIndex();

    void updateByIndex();

    void updateById();

    void removeById();

    void removeByName();

    void removeByIndex();

    void startById();

    void startByIndex();

    void startByName();

    void finishById();

    void finishByIndex();

    void finishByName();

    void changeStatusById();

    void changeStatusByName();

    void changeStatusByIndex();

}
