package ru.tsc.avramenko.tm.api.service;

import ru.tsc.avramenko.tm.model.Project;
import ru.tsc.avramenko.tm.model.Task;

import java.util.List;

public interface IProjectTaskService {

    List<Task> findTaskByProjectId(String projectId);

    Task bindTaskById(String projectId, String taskId);

    Task unbindTaskById(String projectId, String taskId);

    Project removeProjectById(String projectId);

    Project removeProjectByIndex(Integer index);

    Project removeProjectByName(String name);

}